====== Raspberry Pi: System Setup ======

//dumped for now...//

<file text pi3_tablet.txt>
pi3_tablet:
- pi3 with 7in display (https://www.raspberrypi.com/products/raspberry-pi-touch-display/) & enclosure
INSTALL:202408181341
- using raspios-bookworm-arm64 image (with desktop)
- first boot
  = setup user => common!
  = skip update - do that later
- reboot
  = display is inverted (Menu>Preferences>Screen Configuration)
    > right click DSI-1
  = update system (if applicable)
  = customize system (Menu>Preferences>Raspberry Pi Configuration)
    > set hostname
    > enable ssh & i2c
- reboot
  = install: vim geany screen
  = remove: nano
  = touch is independent of display setting (inverted)
    > edit /boot/firmware/config.txt (insert:dtoverlay=vc4-kms-dsi-7inch,invx,invy)
    > also need this (dtparam=i2c_vc_baudrate=50000)
- get my usual stuff from codeberg
  = my1shell, my1linuxpi, my1codelib, my1codebase
  = my1imgpro, my1imgproX
</file>

<file text pi0_config.txt>
- Raspberry Pi OS lite
  = for Pi Zero (bookworm armhf lite)
- write image to sd card, boot
  = english (us) keyboard layout
  = setup username/password
- change hostname (default is raspberrypi)
$ sudo raspi-config nonint do_hostname <hostname>
- change locale (default is en_GB.UTF-8)
$ sudo raspi-config nonint do_change_locale en_US.UTF-8
  = reboot, make sure locale properly set up
- enable ssh
$ sudo raspi-config nonint do_ssh 0
- change timezone
$ sudo timedatectl set-timezone Asia/Kuala_Lumpur

- (optional) if not using wifi (e.g. on pi 1), disable wpa_supplicant
$ sudo systemctl disable wpa_supplicant
- (optional) change wifi country
$ sudo raspi-config nonint do_wifi_country MY
- (optional) disable camera
$ sudo raspi-config nonint do_camera 1
- (optional) running lite most probably do not need much graphics!
$ sudo raspi-config nonint get_config_var gpu_mem /boot/config.txt
$ sudo raspi-config nonint do_memory_split 32
- (optional) run update
$ sudo apt update
$ sudo apt upgrade

- install/remove stuff
$ sudo apt install vim git screen
$ sudo apt remove nano

- get my stuff (git): my1shell, my1linuxpi, my1codelib, my1codebase
</file>
