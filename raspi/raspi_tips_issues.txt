====== Raspberry Pi Tips / Issues ======

Some stuffs...

===== Check if Running Raspbian-Lite =====

To check if we are on lite version, (well one way to do it...) is to check if raspberrypi-ui-mods package is installed. The following command will return nothing if it is not installed - so, we are probably on lite then.
<code>$ sudo apt list raspberrypi-ui-mods --installed</code>

===== Setting Timezone on Pi from commandline =====

I run desktop-less Raspbian (Buster) and somehow the timezone got reset. To set that, <code bash># sudo timedatectl set-timezone Asia/Kuala_Lumpur</code>

===== Change hostname manually =====

Edit ''/etc/hostname'' and ''/etc/hosts'' - modify accordingly, and reboot immediately.

<code bash>
$ name=my1pi
$ prev=$(cat /etc/hostname)
$ sudo sed -i "s/$prev/$name/" /etc/hostname
$ sudo sed -i "s/$prev/$name/" /etc/hosts
$ sudo reboot
</code>

===== Disable Display from Sleeping =====

The simplest way is... install xscreensaver and disable it! <code># sudo apt install xscreensaver</code>

===== ePhoto Frame =====

Script to make Pi an e-Photo Frame.

<file bash eframe.sh>
#!/bin/bash

SHOW_PATH="/home/pi/Pictures"
[ -d "$1" ] && SHOW_PATH=`cd $1;pwd`
SHOW_EXEC="feh"
SHOW_FULL=`which $SHOW_EXEC 2>/dev/null`
# args: delay 5s, fullscreen, recursive, randomize
SHOW_ARGS="-D5 -F -r -z"

echo "Accessing pictures in $SHOW_PATH..."
# press esc to quit?
${SHOW_FULL} ${SHOW_ARGS} ${SHOW_PATH}
</file>

===== Set Static IP =====

  * get the following info: address, netmask, broadcast, network, gateway
    * to get address, netmask, broadcast:<code>
$ ifconfig
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.170.7.195  netmask 255.255.255.0  broadcast 10.170.7.255
</code>
    * to get network, gateway:<code>
$ netstat -nr
Kernel IP routing table
Destination     Gateway         Genmask         Flags   MSS Window  irtt Iface
0.0.0.0         10.170.7.1      0.0.0.0         UG        0 0          0 eth0
10.170.7.0      0.0.0.0         255.255.255.0   U         0 0          0 eth0
</code>
  * edit ''/etc/network/interfaces'' and change <code>iface eth0 inet dhcp</code> to <code>
iface eth0 inet static
address xxx.xxx.xxx.xxx
netmask xxx.xxx.xxx.xxx
network xxx.xxx.xxx.xxx
broadcast xxx.xxx.xxx.xxx
gateway xxx.xxx.xxx.xxx
</code> //note: fill in the info accordingly//

===== Display Issues =====

//**Note** Got these from R-Pi site.//

The HDMI monitor (or HDMI adapter) may only support a limited set of formats for CEA (TV) or DMT (Monitor). To find out which formats are supported, use the following method:

  * Set the output format to VGA 60Hz (hdmi_group=1 and hdmi_mode=1) and boot up your Raspberry Pi
  * Enter the following command to give a list of CEA-supported modes: /opt/vc/bin/tvservice -m CEA
  * Enter the following command to give a list of DMT-supported modes: /opt/vc/bin/tvservice -m DMT
  * Enter the following command to show your current state: /opt/vc/bin/tvservice -s

Then edit config.txt to select:
  * hdmi_group={0:EDID_auto_detect,1:CEA,2:DMT}
  * hdmi_mode={select from tvservice output}

===== Auto-run Script/Program on Startup =====

//**Note**: This assumes a Raspbian installation with desktop environment, and username is **pi**.//

  * create path ''/home/pi/.config/autostart''
  * create a desktop file in there (e.g. monitor.desktop) <file ini monitor.desktop>
[Desktop Entry]
Type=Application
Name=Monitor
Exec=/path/to/exec
</file>

===== Script to run browser full-screen =====

<file bash start_fullscreen.sh>
#!/bin/bash

chromium-browser http://server.local:1337 --start-fullscreen
</file>
