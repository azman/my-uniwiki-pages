====== Lab Work 2 - Combinational Logic Circuits ======

This module consists of a few parts:
  - [[pgt104lab01a|Implementing Combinational Logic]]
  - [[pgt104lab01b|Adders and Comparators]]
  - [[pgt104lab01c|Other Combinational Logic Blocks]]

{{page>pgt104lab01a&noheader&nofooter&noeditbutton}}

<pagebreak>

{{page>pgt104lab01b&noheader&nofooter&noeditbutton}}

<pagebreak>

{{page>pgt104lab01c&noheader&nofooter&noeditbutton}}
