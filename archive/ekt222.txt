====== EKT222 - Microprocessor Systems ======

This course is //Microprocessor Systems//, offered by the School of Computer and Communication Engineering.h

Check out [[https://github.com/my1matrix/my1sim85/wiki|my1sim85]]! Here are some [[http://azman.unimap.edu.my/storage/my1sim85-minipro-201608211239.zip|sample student projects]] developed on my1sim85.

====== Academic Session 2011/2012 ======

This semester (semester 2), I will be handling this course for students in the Computer Engineering programs.

===== Announcement =====

[**20120510**] Assignment 3 (refer below) - due 20120523.

[**20120510**] Assignment 2 (refer below) - due 20120516?

[**20120404**] Assignment 1 (refer below) - due 20120418.

[**20120404**] Mid-Term Examination is set on 20120418 (18 APR, WED) @1400 (2PM)

[**20120210**] Welcome to MY kind of lecture!

===== Timetable =====

Lecture Hours
^  Group  ^  Location  ^  Day  ^  Time  ^
|  (3)  |  ''DKP1''  |  ''WED''  |  ''1000-1200''  |

Lab Hours
^  Group  ^  Location  ^  Day  ^  Time  ^
|  (3)  |  ''MKR1''  |  ''THU''  |  ''1300-1500''  |
|  (3)  |  ''MKR1''  |  ''FRI''  |  ''1000-1200''  |

Consultation Hours //As discussed during lecture 20120307... overridden 20120325//
^  Group  ^  Location  ^  Day  ^  Time  ^
|  ($)  |  ''SME-B10''  |  ''MON''  |  ''1400-1500''  |
|  ($)  |  ''SME-B10''  |  ''TUE''  |  ''1000-1200''  |
|  ($)  |  ''SME-B10''  |  ''WED''  |  ''1500-1600''  |

===== Course Assessment =====

The assessment components are shown below. (//Based on my proposal to the course coordinator...//)

^  ^  Examinations  ^^  Course Work  ^^^
^  Total Contribution  |  70%  ||  30%  |||
^  Assessment  |  Mid-Term Exam  |  Final Exam  |  Quiz/Assignments |  Lab Assessment |  Mini Project  |
^  Contribution  |  20%  |  50%  |  10%  |  5%  |  15%  |

====== Academic Session 2010/2011 ======

This semester (semester 2), I will be handling this course for some of the students in the Computer Engineering, Communication Engineering and Computer Network Engineering programs.

===== Announcement =====

[**20110408**] REMINDER! Test 2 will be held on TUE 20110412 @2030hours! Be prepared!

[**20110315**] Regarding //mark// and //space// in serial communications, see [[http://en.wikipedia.org/wiki/Rs232#Voltage_levels]].

[**20110207**] Test 1 set for 22/02/2011 (TUE at night... time and place not known for now) - I just got this info after today's lecture.

[**20110103**] Welcome to MY kind of lecture!

===== Timetable =====

Lecture Hours
^  Group  ^  Location  ^  Day  ^  Time  ^
|  (Y,Z)  |  ''DKP1''  |  ''MON''  |  ''1000-1200''  |

Lab Hours
^  Group  ^  Location  ^  Day  ^  Time  ^
|  (Y)  |  ''MKR1''  |  ''TUE''  |  ''1100-1300''  |
|  (Y)  |  ''MKR1''  |  ''THU''  |  ''1000-1200''  |

Consultation Hours //As discussed during lecture 20110117...//
^  Group  ^  Location  ^  Day  ^  Time  ^
|  ($)  |  ''SME-B10''  |  ''MON''  |  ''1500-1600''  |
|  ($)  |  ''SME-B10''  |  ''TUE''  |  ''1500-1600''  |
|  ($)  |  ''SME-B10''  |  ''WED''  |  ''0900-1100''  |
|  ($)  |  ''SME-B10''  |  ''FRI''  |  ''0900-1100''  |

<sub>//**Note#1**// Unfortunately, I have to make the 2-hour slot on Friday morning 'by-appointment-only'. If you have any problems with this please come and see me.</sub>

<sub>//**Note#2**// Good news and bad news. The good news is I've added another consultation hour on Wednesday. The bad news is I've made it 'by-appointment-only'. Again, come and see me if you have problems with this.</sub>

===== Lab Assessment =====

We will be using the 8085 simulation software. Assume that ONLY ONE 4KB RAM is available at address 8000H for data (including stack). Assume your code starts at 0000H (the simulator does not support ORG directive!).

Write a SUB-ROUTINE to read an array of data from source address (8000H). For every data read, write a value to destination address (8800H) based on the following rules:

	if data < 50 then value = 0
	if 50 <= data < 200 then value = data
	if data >= 200 then value = 255

The first data in 8000H gives the length of the vector and should not be processed. Copy this value directly to 8800H.

Write a complete program to test your sub-routine with the following data:

	org 8000H
	dfb 10, 25 ,129, 244, 200, 50, 65, 73, 133, 255, 234, 123
@
	dfb 0ah, 19h, 81h, 0f4h, 0c8h, 32h, 41h, 49h, 85h, 0ffh, 0eah, 7bh

Result @8800H should be:

	10, 0, 129, 255, 255, 50, 65, 73, 133, 255, 255
@
	0ah, 00h, 81h, 0ffh, 0ffh, 32h, 41h, 49h, 85h, 0ffh, 0ffh

Submit the complete program.

**Possible Solution**

//Note: I have NOT tested this... but, I think I've covered everything.//

//Note#2: Obviously, you may also use STAX D instead of XCHG'ing HL and DE.//

<code asm assess1.asm>
org 0000h
jmp start

;data - you CAN do this!
org 8000h
source: dfb 0ah, 19h, 81h, 0f4h, 0c8h, 32h, 41h, 49h, 85h, 0ffh, 0eah, 7bh
org 8800h
target: dfs 1 ; dummy

;test code - make sure redefine org!
org 0040h
start:
lxi sp, 9000h
call routine	
hlt

routine:
push h ; just formalities
push d
push b
lxi h, 8000h ; source
lxi d, 8800h ; target
mov c, m ; length in reg c!
inx h ; prepare for data
xchg
mov m, c ; copy first data raw!
inx h
xchg
loop:
mov a, m ; get source data
inx h
xchg
cpi 50
jc make_zero
cpi 200
jc save_this
mvi a, 0ffh ; make_one
jmp save_this
make_zero:
xra a
save_this:
mov m, a
inx h
xchg
dcr c ; check length
jnz loop
pop b
pop d
pop h
ret
</code>
