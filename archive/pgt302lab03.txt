====== Lab Work 4 - Using Embedded OS ======

This module covers working with Raspbian Linux on the Raspberry Pi. We will see how we can access the hardware resources from within Raspbian. We will also see how we can utilize a web server running on Pi.

====== Preparing Raspbian ======

{{page>raspi:raspi_prepare_raspbian&noheader}}

====== Running Codes in my1linuxpi ======

//coming soon...//

====== Archive ======

Some old notes.

===== Starting NOOBS: Installing Raspbian =====

//**Note**: Since this took too much time to be demonstrated during a lab session, NOOBS will no longer be considered/discussed. However, it is still available on [[https://www.raspberrypi.org/downloads/|the official download page]]. This is left here for reference.//

Installation of the OS actually involves in creating more partitions for boot, root and optionally data - the original single partition is reserved/maintained as 'recovery' partition. This is my fdisk output for the NOOBS card AFTER finishing the installation.

<code>
Disk /dev/sdc: 7892 MB, 7892631552 bytes
4 heads, 16 sectors/track, 240864 cylinders, total 15415296 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x000031bb

   Device Boot      Start         End      Blocks   Id  System
/dev/sdc1            8192     3011718     1501763+   e  W95 FAT16 (LBA)
/dev/sdc2         3014656    15349759     6167552   85  Linux extended
/dev/sdc3        15349760    15415295       32768   83  Linux
/dev/sdc5         3022848     3145727       61440    c  W95 FAT32 (LBA)
/dev/sdc6         3153920    14292991     5569536   83  Linux
/dev/sdc7        14295040    15343615      524288   83  Linux
</code>

The recovery partition is at /dev/sdc1 (notice that it is using FAT16 partition). The boot partition is in /dev/sdc5, which is using FAT32. The root partition is at /dev/sdc6, while a data partition is created on /dev/sdc7. Note that /dev/sdc{5,6,7} are all logical partitions made from primary partition at /dev/sdc2 (look at the sector address Start and End). Finally, partition /dev/sdc3 is where NOOBS keeps its configuration files.

Your instructor will give a demonstration on how NOOBS is used to configure an OS for Pi.

===== Proceeding with (ONLY) Raspbian =====

//**Note**: Raspbian has grown considerable since this was written, and no longer suitable for my lab session.//

Using NOOBS is nice, but somehow Pi simply will not boot without a keyboard attached. Installing [[https://www.raspberrypi.org/downloads/raspbian/|Raspbian]] image (and only Raspbian) on the (micro)SD card is supposed to remedy that problem. It is a zipped image file which is supposed to be written directly to the card (using dd on Linux). You can get a manual on how to work with this image file from [[https://www.raspberrypi.org/documentation/installation/installing-images/README.md|here]].

If you have problems downloading 900+MB zip file (like me... it takes forever!), we can actually extract the relevant files from the NOOBS card. Look into the recovery partition, and find two files from folder 'raspbian' - boot.tar.xz and root.tar.xz - which are the only files we need. I created two partitions:

  * boot partition (FAT32) - extract [[http://azman.unimap.edu.my/storage/raspbian_noobs_1.3.9-boot.tar.xz|boot.tar.xz]] ([[http://azman.unimap.edu.my/storage/raspbian_noobs_1.3.9-boot.tar.xz.md5|MD5]]) into this partition
  * root partition (ext4) - extract (duh!) [[http://azman.unimap.edu.my/storage/raspbian_noobs_1.3.9-root.tar.xz|root.tar.xz]] ([[http://azman.unimap.edu.my/storage/raspbian_noobs_1.3.9-root.tar.xz.md5|MD5]]) into this one

Note that those files available locally on our server were extracted from the older NOOBS 1.3.9. Newer versions are usually larger in size and I want to keep it small - this version is the one that came with the SD Card purchased along with the board.

When you first boot up to Raspbian, it will run raspbian-config (allows you to configure a few things before starting up). As mentioned earlier, we can set Raspbian to boot to Scratch here, but the other thing that I would recommend to have is to start the SSH server running. This will allow us to remotely login through SSH (a network is obviously required) in case we do not have a suitable display 8-)
