====== NMK20103 - Microprocessor ======

//**Note20241219** This page is being updated - work in progress...//

This course is //Microprocessor//, offered by the Faculty of Electronics Engineering & Technology.

===== Announcements =====

[20241219] Updating this for 202425s1 Academic Session.

===== Resources =====

Lecture Slide(s):
  * {{:archive:nmk201:serial_comm.pdf|Serial Communications}}

Reference(s):
  * 8085 ({{:archive:nmk201:i8085_datasheet.pdf|Datasheet}})
  * 8085 Instructions Set ({{:archive:nmk201:i8085_instruction_set.pdf|full}})({{ :archive:nmk201:i8085_instruction_set_summary.pdf |1-page summary table}}))
  * 8085 Internal Architecture ({{:archive:nmk201:i8085_arch.jpg?linkonly|schematic}})
