====== Javascript Programming Notes ======

I do some Javascript programming from time to time. Just though I should start keeping some useful notes for my future reference (sometimes I waste time looking up something I have actually gone through :p).

===== Variable Declaration =====

I always declare ''use strict'' and use ''var'' to declare a variable. However I just found out alternatives: ''[[https://www.w3schools.com/JS/js_let.asp|let]]'' and ''const'', which is more relatable to my background in C programming. Both those options comes with ''block scope'' - before ES6(2015), Javascript only supports ''global scope'' and ''function scope''. So, I will start to use ''let'' instead of ''var'' from now on.
