====== Using rclone to sync Google Drive on Linux ======

Dumping this here for now...

<code>
- configure drive <remote>
$ rclone config
  = can select specific folder as <remote>
  = folderid is last part of url
- list all top level directories
$ rclone lsd <remote>:
- list all files
$ rclone ls <remote>:
- copy things from local to folder that in <remote>
$ rclone copy /local/things <remote>:that
  = can use sync instead?
- copy things under folder that in <remote> to local path
$ rclone copy <remote>:that /local/that/
  = path /local/that must be created locally!
# use '--drive-export-formats ods,odt,odp' to convert to libreoffice format instead
</code>
