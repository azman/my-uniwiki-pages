====== Slackware Notes Archive ======

Slackware installation and setup has improved tremendously over the years since I first use it circa 2006 (Slackware 11 @ current at that time). Most of the notes I have are no longer needed, so I have decided to move them here for personal archive.

===== Nice to have(s) =====

For my personal reference...

  * [[http://docs.slackware.com/howtos:network_services:roaming_profiles|Roaming Profiles]]

===== Sample Installation =====

Installing Slackware 14.1 on my HP EliteDesk 800 G1 Tower machine @workplace.

  - fresh and full install
    * well, actually without software from e, f, kdei and y (personal taste)
    * as usual, no problems during installation... reboot
  - rebooted to runlevel 3 without any problems
    * login using root and created main user account
    * if using lilo
      * modify /etc/lilo.conf (reduced timeout, lba32, extra boot option for resume)
      * rerun lilo
    * modified /etc/inittab to init @ runlevel 4
    * added ''echo 0 > /proc/sys/kernel/dmesg_restrict'' to ''rc.local''
  - rebooted to runlevel 4 without any problems
    * login using user account and made customizations as desired

===== System Setup =====

  - recompile kernel (customized)
    * initrd not needed (if we need a particular driver, compile into custom kernel)
    * for my AMD64 desktop machine with 3GB RAM, i just use the huge-smp kernel and be done with!
  - reconfigure lilo to boot newly compiled kernel  
    * keep huge-smp kernel in the list for safe-mode boot
    * skip this if you're NOT recompiling the kernel
  - modify xorg.conf as needed
    * run xorgconfig if needed (else just edit or copy old conf)
    * install binary graphics driver if needed (e.g. nvidia)
    * scrolling usb mouse sometimes needs to be configured manually?
    * the new Slackware 13 (with KDE4) seems to NOT need this anymore
      * unless maybe if you want to install something like nvidia driver
  - add user accounts
    * use adduser (hit arrow up key for additional group)
    * groups: ''users floppy dialout audio video cdrom plugdev power netdev''
    * make separate accounts for archiving (without $HOME)
  - configure alsa
    * no sound by default: use [alsaconf (if needed), alsamixer, alsactl store] combo
    * seems like not needed in slack 13.0 anymore
  - configure ssh remote login
    * set ''PermitRootLogin no'' in sshd.config
    * or, just simply disable rc.sshd if you don't need it
  - give user home folder more privacy
    * ''chmod 700 /home/*''... or maybe chmod 711? this is a matter of choice
  - modify Xserver settings?
    * add ''%% --nolisten tcp %%'' to default settings
    * never done this.. so, no comment on this
  - consider lvm?
    * never done this.. so, no comment on this
  - setleds & ntpdate in rc.local?
    * never done this.. so, no comment on this

===== System Security =====

Hardly needed these... maybe if we need to be that paranoid? 8-o

  - edit the hosts* files to control access
    * deny access to all hosts by default - edit ''/etc/hosts.deny'' <code>ALL: ALL</code>
    * back-up defense - edit ''/etc/hosts.allow'' <code>ALL: ALL: DENY</code>
    * or, punch a hole for certain //secured// services for **ALL** hosts -  edit ''/etc/hosts.allow'' <code>sshd: ALL: ALLOW</code>
  - having a basic firewall is handy:
    * create an ''rc.firewall'' script: <code># default policies - drop everything!
iptables -P INPUT DROP
iptables -P FORWARD DROP

# added (-A) policies for INPUT:
# say ok to previously established connections
# and always allow loopback interface and ssh connections
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A INPUT -s 127.0.0.1 -d 127.0.0.1 -i lo -j ACCEPT
iptables -A INPUT -p tcp --dport 22 -i eth0 -j ACCEPT
</code>
  - disable unused services
    * unset the executable bit of the respective startup scripts in ''/etc/rc.d''
  - checkout ''/etc/ssh/sshd_config'' and configure things like disabling ''PermitRootLogin''
  - minimize ''suid'' programs:
    * look for them ''find / -perm +4000 2>/dev/null''
  - edit ''/etc/securetty'' - control root login devices
  - for the extra security - set a umask of 077! 

===== ACPI: ASUS eeepc X101H =====

Dumped here for now... will prettify this :p

<file txt dump.txt>
* all special fn-keys will trigger acpi event group PNP0C14:00 action 000000d2

# fn-f1 => sleep button
  - group: button
  - action: sleep
  - *note* triggers 2 events SBTN and PNP0C0E:00 (sleep/wakeup?)
  - *note* this does not trigger PNP0C14:00 event!

# fn-f2 => toggle wireless (rfkill?)
  - group: button
  - action: wlan

# fn-f3 => toggle touchpad
  - *note* only triggers PNP0C14:00 event

# fn-f4 => ??? full screen resolution ???
  - *note* only triggers PNP0C14:00 event

# fn-f5 => decrease brightness
  - group: video
  - action: brightnessdown

# fn-f6 => increase brightness
  - group: video
  - action: brightnessup

# fn-f7 => display off
  - *note* only triggers PNP0C14:00 event

# fn-f8 => switch display
  - group: video
  - action: switchmode

# fn-f9 => task manager
  - group: button
  - action: prog1

# fn-f10 => toggle mute volume
  - group: button
  - action: mute

# fn-f11 => decrease volume
  - group: button
  - action: volumedown

# fn-f12 => increase volume
  - group: button
  - action: volumeup

# fn-<space> => super hybrid engine
  - *note* only triggers PNP0C14:00 event

asus hotkey (action ASUS010:00)

00000039 - (sb) super hybrid engine
00000038 - (f4) full screen?
00000037 - (f3) enable/disable touchpad
00000030 - (f8) switch display (acpi compatible)

00000023 - (f6) brightness up
00000022 - (f5) brightness down

00000016 - (f7) display off (acpi compatible)
00000015 - (f12) vol up
00000014 - (f11) vol down
00000013 - (f10) mute
00000012 - (f9) prog1 (acpi compatible)
00000010 - (f2) wlan
</file>

===== Sharing Maxis Broadband on Wireless ADSL Router =====

I have a USB device for wireless broadband connection for 3G/HSDPA network. I wanted to share it using my existing wireless ADSL router. I have a Slackware 12.2 machine with a free eth0 device (since using USB to go on the internet). Here's what I did:
  * Disable DHCP server on router, change PPPoE to dynamic IP
    * Log in to router, find the LAN DHCP option  and disable it
    * Find internet IP option (for ADSL, it's PPPoE), and change it to dynamic
  * On Slackware, start ip forwarding and DHCP server<code bash>
/etc/rc.d/rc.ip_forwarding start
/sbin/dhcpd eth0
</code>
  * Get a firewall script (iptables) from Alien_Bob's [[http://alien.slackbook.org/dokuwiki/doku.php|site]]
That's about it. Now both my wife and I can go online at the same time :p

**//Note://** This can also be set for a basic wireless AP. In fact, it's much simpler.

===== DVD+R Burning on Slackware 13.0 =====

I've been having problems burning DVD+Rs on Linux (Slackware 13.0). Apparently, the dvd+rw-tools has a certain [[http://fy.chalmers.se/~appro/linux/DVD+RW/tools/|bug]] that makes the DVD+R not properly //finalized//. So, just download the code snippet mentioned on that page which is actually a C++ source file (''[[http://fy.chalmers.se/%7Eappro/linux/DVD+RW/close.cpp|close.cpp]]'') along with the main dvd+rw-tools source tarball (I used 7.1 - which is the version installed on Slack 13.0). Extract the tarball and copy the file (''close.cpp'') into the source location. Compiling as it is will generate an error regarding INT_MAX not defined. So, I just insert <code>#include <limits.h></code> in ''transport.hxx'' (@line 142). Then, to compile: <code>g++ -o close-dvdr close.cpp</code>

Use that to finalize the DVD+R <code>./close-dvdr /dev/dvd</code>

Everything will be okay then!

===== Synaptics Touch Pad on Slack 13.0 =====

Slack 13 (with KDE4) doesn't really need xorg.conf any longer - unless we install something like nVidia graphics driver. So, when I found out that the touch pad on my laptop is not working, I looked around and found that I have to use HAL for this. There's already a sample FDI file - ''/usr/share/hal/fdi/policy/10osvendor/11-x11-synaptics.fdi''. So, we just copy this file into ''/etc/hal/fdi/policy'' and edit it accordingly. Neat!

===== Linux-PPTP =====

This is a setup for Wireless Connection to a (MS?) VPN server on Slackware 12.0.
<code>
Wireless Access Point SSID: SNAP
Encryption: WEP
Encryption Key: 3132333435 (hex)
VPN Server IP: 130.95.11.2
VPN Username & Password!: <YOU> & <WISH>
</code>

Get Connected to the Wireless AP

  * get access point (AP) details: (i) SSID (ii) encryption type/key
  * possibly using DHCP nowadays
  * assuming device setup as eth1
  * ''# iwconfig eth1 essid "SNAP" enc 3132333435''
  * ''# dhcpcd -t 5 eth1''
  * to kill later (if necessary), use -k option
  * take note netmask & gateway settings

Create VPN connection with the server

  * needs pptp-linux package - get it from [[http://rlworkman.net/pkgs|here]]
  * get the VPN server IP ready
  * we need username & password for this!
  * create tunnel using ''# pppd call <tunnel>''

We also need the file <tunnel> in ''/etc/ppp/peers'':

<code>
# file: /etc/ppp/peers/testsnap
pty "pptp 130.95.11.2 --nolaunchpppd"
name <YOU>
#mtu 1490
#mru 1490
mtu 1200
mru 1200
remotename PPTP
#require-mschap
require-mschap-v2
require-mppe-128
file /etc/ppp/options.pptp
ipparam testsnap
persist
</code>

And the password in ''/etc/ppp/chap-secrets'':

<code>
# Secrets for authentication using CHAP
# client	server	secret			IP addresses
<YOU>		PPTP	<WISH>			*
</code>

Update routing table

  * redirect all packets to VPN server through the tunnel
  * ''# route add -host 130.95.11.2 dev ppp0''
  * add new default route on tunnel device
  * ''# route add default dev ppp0''
  * optionally, delete default the existing default route (wireless connection)
  * ''# route del default eth1''

//**Note** I've been trying to configure VPN on UWA network - I managed to get everything running, except for browsing Google pages. Today Martin Geier managed to solve my problem - it turns out the MTU setting (1490 - default) is too high? With MTU set at 1200, everything is perfect! He said the problem would be at the VPN server, which might have made the automatic packet resizing screwed up! Anyways, I'm happy that I had that solved! Viva Martin! //

===== Linux Wireless =====

**Dell Latitude E5400**

In pursuit of converting my family to using Linux, I managed to persuade my wife to install Slackware Linux on her new Dell Latitude laptop. The installation went well and everything was fine except for wireless and the touchpad. The touchpad is easily fixed either by using xorg.conf (which is not really used anymore) or by using an fdi policy. Will talk about that some other time.

<code>
Laptop Model: Dell Inspiron
Wireless Device: Dell WLAN 1510 (802.11a/b/g/n) MiniCard
Pointer Device: Dual Pointing without Fingerprint Reader with Bluetooth for Malaysia only
Graphics: Nvidia GeForce 9200M GS discrete graphics w/256M DDR3 memory (just put his here for now)
</code>

Using //''lspci''// The wireless device was detected as

<code>
0c:00.0 Network controller: Broadcom Corporation BCM4322 802.11a/b/g/n Wireless LAN Controller (rev 01)
</code>

This is a big problem because the stock modules that come with the kernel doesn't work. The open source driver at [[http://linuxwireless.org/en/users/Drivers/b43/|linuxwireless.org]] doesn't work either. I had to rely on the closed-source driver provided by Broadcom ([[http://www.broadcom.com/support/802.11/linux_sta.php|broadcom-sta]]). Slackbuilds.org is a life-saver again by providing a [[http://slackbuilds.org/repository/13.0/network/broadcom-sta/|build script]] for this. Since Slack 13.0 somehow didn't automatically load the stock b43 module, I didn't have to blacklist anything. It just works after that!

**Update20100831** On Slack64 13.1, I had to blacklist b43 and ssb.

===== Linux Modem =====

**Compaq NX6120 Internal Dial-Up Modem**

<code>
mknod -m 660 /dev/slamr$(minor) c 242 $(minor)
modprobe -r slamr
/usr/sbin/slmodemd --country=MALAYSIA /dev/slamr0
</code>

===== DVD Stuff =====

To create ISO image:
<code>dd if=/dev/dvd of=dvd-image.iso bs=2048</code>

To burn DVD image:
<code>growisofs -dvd-compat -Z /dev/dvd=dvd-image.iso</code>

Check the ISO size:
<code bash>
$ echo $(( $(ls -l dvd-image.iso | awk '{ print $5 }') / 2048 ))
169383
</code>

Get the ISO's MD5 checksum:
<code bash>
$ cat dvd-image.iso | md5sum
cc363de222ba6fe7455258e72b6c26ca  -
</code>

Checked the burned DVD's MD5 checksum:
<code bash>
$ dd if=/dev/dvd bs=2048 count=169383 | md5sum
169383+0 records in
169383+0 records out
cc363de222ba6fe7455258e72b6c26ca  -
</code>
